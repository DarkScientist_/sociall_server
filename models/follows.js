'use strict';
module.exports = (sequelize, DataTypes) => {
  const follows = sequelize.define('Follow', {
    follow_id: DataTypes.INTEGER,
    followed_id: DataTypes.INTEGER
  }, {});
  follows.associate = function(models) {
    // associations can be defined here
    models.Follow.belongsTo(models.User, {
      foreignKey: {
        allowNull: false
      }
    });
  };
  return follows;
};