'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    profile: DataTypes.STRING,
    age: DataTypes.INTEGER,
    cat: DataTypes.STRING,
    punishments: DataTypes.INTEGER,
    follow: DataTypes.STRING,
    followed: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN
  }, {});
  User.associate = function(models) {
    // associations can be defined here

    models.User.hasMany(models.Message);
  };
  return User;
};