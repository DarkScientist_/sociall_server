var jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = 'toadd';


module.exports = {
    generateTokenForUser : function (userData) {

        return jwt.sign({
            userId: userData.id,
            username: userData.username,
            isAdmin: userData.isAdmin

        },
        JWT_SIGN_SECRET,
        {
            expiresIn: '1h'
        }
        )
        
    },

    parseAuthorization: function(authorization) {
        return (authorization != null) ? authorization.replace('Bearer ', '') : null;
      },
      getUserId: function(authorization) {
        var userId = -1;
        var username = null;
        var token = module.exports.parseAuthorization(authorization);
        if(token != null) {
          try {
            var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
            if(jwtToken != null)
              userId = jwtToken.userId;
              username = jwtToken.username;
          } catch(err) { }
        }
        return userId;
      }
    }

