var express = require('express');
const Sequelize = require('sequelize');
var server = express();
var redis   = require("redis");
var session = require('express-session');
var redisStore = require('connect-redis')(session);
var client  = redis.createClient();
var bodyParser = require('body-parser');
var apiRouter = require('./apiRouter').router;

const sequelize = new Sequelize('mysql://root@localhost:3306/database_development_sociall');

server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());

//Routes

server.get('/', function (req, res) {

    res.setHeader('ContentType', 'text/html');
    res.status(200).send('<h1>Sociall auth server</h1>');
   
    
});

server.use(session({
  secret: 'ssshhhhh',
  // create new redis store.
  store: new redisStore({ host: 'localhost', port: 6379, client: client,ttl :  260}),
  saveUninitialized: false,
  maxAge: null
  
  
})); 

server.use('/api/', apiRouter);


//Server

server.listen(8080, function () {
    console.log('Server started')
})

//Test Sequelize
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });