//Imports
var models = require('../models');
var asyncLib = require('async');
var jwtUtils = require('../utils/jwt.utils');

//Constants
const TITLE_LIMIT = 2;
const CONTENT_TITLE = 4;

//Route
module.exports= {
    createMessage: function (req, res) {
        
        if(req.session.key){

            
        //Params
        var title = req.body.title;
        var content = req.body.content;
        var category = req.body.category;
        var attach = req.body.attach;
        

        if (title == null || content == null || category == null) {
            return res.status(400).json({'error' :  'missing parameters'});
        }

        if (title.length <= TITLE_LIMIT || content.length <= CONTENT_TITLE) {
            return res.status(400).json({'error' : 'Title or content must be and 4 char minimum'});
        }

        if(attach == null){
            attach = 'noattach';
        }
        asyncLib.waterfall([
            function (done) {
                models.User.findOne({
                    where: {id: req.session.key}
                })
                .then(function (userFound) {
                    done(null, userFound)
                })  
                .catch(function (err) {
                    return res.status(500).json({'error' : 'unable to verify user'});
                    
                });
            },
            function (userFound, done) {
                if (userFound) {
                    models.Message.create({
                        title: title,
                        content: content,
                        category: category,
                        attach: attach,
                        likes: 0,
                        UserId: userFound.id
                        
                    })
                    .then(function (newMessage) {
                        done(newMessage);
                    })
                }
            }

        ], function (newMessage) {
            if (newMessage) {
                return res.status(201).json(newMessage);
            } else {
                return res.status(500).json({'error' : 'cannot post message'});
            }
        })
    } else {
        return res.status(404).json({'error' : 'user not found'});

    }
    },
    listMessages: function (req, res) {
        var fields = req.query.fields;
        var category = req.query.category;
        var limit = parseInt(req.query.limit);
        var offset = parseInt(req.query.offset);
        var order = req.query.order;

        models.Message.findAll({
            order: [(order != null) ? order.split(':') : ['title', 'ASC']],
            attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,
            limit: (!isNaN(limit)) ? limit : null,
            offset: (!isNaN(offset)) ? offset : null,
            where: {category: category},
            include: [{
                model: models.User,
                attributes: [ 'username' ]
      }]
        })
        .then(function(messages) {
            if (messages) {
              return res.status(200).json(messages);
            } else {
              return res.status(404).json({ "error": "no messages found" });
            }
          }).catch(function(err) {
            console.log(err);
            return res.status(500).json({ "error": "invalid fields" });
          });

        
    }
}