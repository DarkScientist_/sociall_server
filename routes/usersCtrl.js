//Require

var bcrypt = require('bcrypt');
var jwtUtils = require('../utils/jwt.utils');
var models  = require('../models');
var asyncLib = require('async');
var server = require('../server')
var session = require('express-session');
var redisStore = require('connect-redis')(session);


//Consts

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=[^\d_].*?\d)\w(\w|[!@#$%]){7,20}/;

module.exports = {
    register: function (req, res) {

        //Params

        var email = req.body.email;
        var username = req.body.username;
        var password = req.body.password;
        var age = req.body.age;
        var cat = req.body.cat;
        var profile = req.body.profile;

        if (email ==null || username == null || password == null || age == null || cat == null)  {
            return res.status(400).json({'error' : 'missing parameters'});
        }

        //Verifs
        //Password
        if(username.length >= 15 || username.length <= 4){

            return res.status(400).json({'error' : 'invalid username'});


        }
        //Email
        if (!EMAIL_REGEX.test(email)) {
            return res.status(400).json({'error' : 'invalid email'});

            
        }
        //Password
        if (!PASSWORD_REGEX.test(password)) {
            return res.status(400).json({'error' : 'invalid password'});

            
        }

        //WaterFall

        asyncLib.waterfall([
            function(done) {
                models.User.findOne({
                    attributes: ['email'],
                    where: {email : email}
                })
                .then(function (userFound) {
                    done(null, userFound);
                })
                .catch(function (err) {

                    return res.status(500).json({'error' : 'unable to verify user'});
                    
                })
            },
            function(userFound, done) {
               
                if (!userFound) {

                    bcrypt.hash(password, 5, function(err, bcryptedPassword){
                        done(null, userFound, bcryptedPassword);
                    
                    });
                    
                } else {
                    return res.status(409).json('error : user already exists');
                }
                    

                
                },
                function (userFound, bcryptedPassword, done) {

                    var newUser = models.User.create({
                        email: email,
                        username: username,
                        password: bcryptedPassword,
                        profile: profile,
                        age: age,
                        cat: cat,
                        isAdmin: 0
                    })
                    .then(function (newUser) {
                        done(newUser);
                    })
                    .catch(function (err) {
                        return res.status(500).json({'error' : 'unable to add user'});
                    });
                    
                }
        ], function (newUser) {
            // result now equals 'done'
            if (newUser) {
                return res.status(201).json({
                    'userId' : newUser.id
                });
                
            } else {

                return res.status(500).json({'error' : 'unable to add user'});
                
            }
        });





        
    },

    login: function (req, res) {

        var email = req.body.email;
        var password = req.body.password;
        
        if (email == null || password == null) {

            return res.status(400).json({'error' : 'missing parameters'});
            
        }
        if(!req.session.key){
        asyncLib.waterfall([
            function(done) {
              models.User.findOne({
                where: { email: email }
              })
              .then(function(userFound) {
                done(null, userFound);
              })
              .catch(function(err) {
                return res.status(500).json({ 'error': 'unable to verify user' });
              });
            },
            function(userFound, done) {
              if (userFound) {
                bcrypt.compare(password, userFound.password, function(errBycrypt, resBycrypt) {
                  done(null, userFound, resBycrypt);
                });
              } else {
                return res.status(404).json({ 'error': 'user not exist in DB' });
              }
            },
            function(userFound, resBycrypt, done) {
              if(resBycrypt) {
                done(userFound);
              } else {
                return res.status(403).json({ 'error': 'invalid password' });
              }
            }
          ], function(userFound) {
            if (userFound) {

              req.session.key = userFound.id;
              console.log(req.session.key);
              return res.status(201).json({
                'userId': userFound.id,
                
              });

              
            } else {
              return res.status(500).json({ 'error': 'cannot log on user' });
            }

          
          });
        } else {
          return res.status(400).json({'error' : 'already logged in.'});
        }
    },
    //Get User Profile
    getUserProfile: function(req, res) {
        
      if(req.session.key) {
        // if email key is sent redirect.
        var email = req.session.key;

        models.User.findOne({
          attributes: [ 'id', 'email', 'username', 'profile' ],
          where: { email: email }
        }).then(function(user) {
          if (user) {
            res.status(201).json(user);
          } else {
            res.status(404).json({ 'error': 'user not found' });
          }
        }).catch(function(err) {
          res.status(500).json({ 'error': 'cannot fetch user' });
        });

        console.log(models.User.rawAttributes.follow.values);
        
      
      } else {
        // else go to home page.
        return res.status(404).json({'error' : 'not logged in'})
    }  
      
      
        
        
    
  },
    
        
    
        //Update user profile
    updateUserProfile: function (req, res) {
                
                if(req.session.key){
                //Params
                var email = req.session.key;
                var profile = req.body.profile;
                asyncLib.waterfall([
                    function(done) {
                      models.User.findOne({
                        attributes: ['id', 'profile'],
                        where: { email: email }
                      }).then(function (userFound) {
                        done(null, userFound);
                      })
                      .catch(function(err) {
                        return res.status(500).json({ 'error': 'unable to verify user' });
                      });
                    },
                    function(userFound, done) {
                      if(userFound) {
                        userFound.update({
                          profile: (profile ? profile : userFound.profile)
                        }).then(function() {
                          done(userFound);
                        }).catch(function(err) {
                          res.status(500).json({ 'error': 'cannot update user' });
                        });
                      } else {
                      }
                    },
                  ], function(userFound) {
                    if (userFound) {
                      return res.status(201).json(userFound);
                    } else {
                      return res.status(500).json({ 'error': 'cannot update user profile' });
                    }
                  });
                } else {
                  res.status(404).json({ 'error': 'user not found' });

                }
        
    },

    logout: function (req, res) {
      req.session.destroy(function(err){
        if(err){
            console.log(err);
        } else {
          res.clearCookie("connect.sid");

            res.redirect('/api/users/');
        }
    });
    },

    followUser: function (req, res) {

      if(req.session.key){

            
        //Params
        var username = req.params.username;
        

        

        
        asyncLib.waterfall([
            function (done) {
                models.User.findOne({
                    where: {username : username}
                })
                .then(function (userFound) {
                    done(null, userFound)
                })  
                .catch(function (err) {
                    return res.status(500).json({'error' : 'unable to verify user'});
                    
                });
            },
            function (userFound, done) {
                if (userFound) {
                    models.Follow.create({
                        
                        UserId: req.session.key,
                        followed_id: userFound.id
                        
                    })
                    .then(function (addFriend) {
                        done(addFriend);
                    })
                } else {
                  return res.status(404).json({'error' : 'user to follow not found'});
                }
            }

        ], function (addFriend) {
            if (addFriend) {
                return res.status(201).json(addFriend);
            } else {
                return res.status(500).json({'error' : 'cannot add friend'});
            }
        })
    } else {
        return res.status(404).json({'error' : 'user not found'});

    }

      
    
      
    }
    
    
}