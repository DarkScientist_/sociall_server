var express = require('express');
var usersCtrl = require('./routes/usersCtrl');
var likesCtrl = require('./routes/likesCtrl');
var messagesCtrl = require('./routes/messagesCtrl');
var commentsCtrl = require('./routes/commentsCtrl');


exports.router = (function () {
    var apiRouter = express.Router();


    
    //Users routes
    apiRouter.route('/users/register/').post(usersCtrl.register);
    apiRouter.route('/users/login/').post(usersCtrl.login);
    apiRouter.route('/users/me').get(usersCtrl.getUserProfile);
    apiRouter.route('/users/me').put(usersCtrl.updateUserProfile);
    apiRouter.route('/users/logout').get(usersCtrl.logout);
    apiRouter.route('/users/follow/:username').post(usersCtrl.followUser);
    
    //Messages routes
    apiRouter.route('/messages/new').post(messagesCtrl.createMessage);
    apiRouter.route('/messages/').get(messagesCtrl.listMessages);

     // Likes
    apiRouter.route('/messages/:messageId/vote/like').post(likesCtrl.likePost);
    apiRouter.route('/messages/:messageId/vote/dislike').post(likesCtrl.dislikePost);
    apiRouter.route('/messages/:messageId/vote/get').get(likesCtrl.getWhoLiked);

    //Comments
    apiRouter.route('/messages/:messageId/comment/new').post(commentsCtrl.postComment);
    

    return apiRouter;

})();
